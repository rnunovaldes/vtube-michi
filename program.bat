@ECHO off
@chcp 65001>nul

SET "FILES_TO_CHECK=Main Constants engine\Observable engine\Observer engine\DeltaTimeManager engine\detectors\Keyboard engine\detectors\Microphone engine\detectors\Mouse engine\view\Window"
SET "EXEC=main.Main"
SET "JAVA_PRIN=src\main\Main.java"

REM Pregunta al usuario si desea compilar
choice /c SN /n /m "¿Compilar (S/N)?"
IF ERRORLEVEL == 2 GOTO CON
if ERRORLEVEL == 1 GOTO COM

REM Verifica la existencia de archivos .class
:CON
FOR %%F IN (%FILES_TO_CHECK%) DO (
    IF NOT EXIST "bin\main\%%F.class" (
        ECHO El archivo "%%F.class" no se encontró en la carpeta bin.
        GOTO COM
    )
)
GOTO EXE

REM Compilar si se ha seleccionado
:COM
IF NOT EXIST bin MKDIR bin
ECHO Vamos a compilar
javac -encoding UTF-8 -d bin -cp lib/*;src %JAVA_PRIN%

REM Ejecutar el programa compilado
:EXE
ECHO Vamos a ejecutar el compilado
java -cp bin;lib/* %EXEC%
PAUSE

REM Eliminar la carpeta con los archivos .class
ECHO Se eliminara la carpeta con los class
RMDIR /s bin
EXIT
