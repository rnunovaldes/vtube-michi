#!/bin/bash

function jumpto {
  label=$1
  cmd=$(sed -n "/$label:/{:a;n;p;ba};" $0 | grep -v ':$')
  eval "$cmd"
  exit
}

function pause {
  read -s -n 1 -p "Presione alguna tecla para continuar . . ."
  echo ""
}

FILES_TO_CHECK="Main Constants engine/Observable engine/Observer engine/DeltaTimeManager engine/detectors/Keyboard engine/detectors/Microphone engine/detectors/Mouse engine/view/Window"
EXEC="main.Main"
JAVA_PRIN="src/main/Main.java"

# Pregunta al usuario si desea compilar
while true; do
  read -p "¿Compilar? (S/N)? " yn
  if [[ "$yn" =~ [sS] ]]; then jumpto comp; fi
  if [[ "$yn" =~ [nN] ]]; then jumpto cont; fi
  echo echo "Opción inválida. Por favor, seleccione S o N."
done

# Verifica la existencia de archivos .class
cont:
for FILE in $FILES_TO_CHECK; do
  if [[ ! -f "bin/main/$FILE.class" ]]; then
    echo "El archivo \"$FILE.class\" no se encontró en la carpeta bin."
    jumpto comp
  fi
done
jumpto exe

# Compilar si se ha seleccionado
comp:
if [[ ! -d ./bin ]]; then mkdir bin; fi
echo "Vamos a compilar"
javac -encoding UTF-8 -d bin -cp lib/*:src "$JAVA_PRIN"

# Ejecutar el programa compilado
exe:
echo "Vamos a ejecutar el compilado"
java -cp bin:lib/* "$EXEC"
pause

# Eliminar la carpeta con los archivos .class
echo "Se eliminará la carpeta con los archivos .class"
rm -rI ./bin
exit
