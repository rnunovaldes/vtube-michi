package main;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.util.Properties;

/**
 * Contiene valores constantes que son relevantes para varias partes
 * del programa.
 */
public class Constants {

    /** El título del programa. */
    public static final String TITLE = "SheIsRaissa";

    /**
     * Tipo de separación usada por el sistema operativo para navegar
     * por su sistema de archivos.
     */
    public static final String SEP = System.getProperty("file.separator");
    /** Ruta relativa del directorio de recursos. */
    public static final String RES = "." + Constants.SEP + "res" + Constants.SEP;
    /** Ruta relativa del directorio de salida. */
    public static final String OUT = "." + Constants.SEP + "out" + Constants.SEP;
    /** Ruta del archivo de configuración. */
    public static final String CONFIG_FILE = RES + "config.properties";

    /**
     * Dimensiones de la pantalla.
     */
    private static final Dimension SCREEN_SIZE =
        Toolkit.getDefaultToolkit().getScreenSize();
    /** Ancho de la pantalla. */
    public static final int FULLSCREEN_WIDTH = (int) SCREEN_SIZE.getWidth();
    /** Altura de la pantalla. */
    public static final int FULLSCREEN_HEIGHT = (int) SCREEN_SIZE.getHeight();

    /** Ruta de los recursos visuales. */
    public final static String SKIN_ROUTE = RES + "skin.png",
                               TABLE_ROUTE = RES + "table.png",
                               MOUSE_ROUTE = RES + "mouse.png",
                               HANDMOUSE_ROUTE = RES + "handmouse.png",
                               HANDKEYBOARD_ROUTE = RES + "handkeyboard.png",
                               LAPTOP_ROUTE = RES + "laptop.png",
                               MOUTH_ROUTE = RES + "mouth.png",
                               HAIR_ROUTE = RES + "hair.png",
                               FACE_1 = RES + "face-1.png",
                               FACE_2 = RES + "face-2.png";

    /** Arreglo de rutas de los recursos. */
    public final static String[] ROUTES = {
            SKIN_ROUTE, TABLE_ROUTE, MOUSE_ROUTE, HANDMOUSE_ROUTE,
            HANDKEYBOARD_ROUTE, MOUTH_ROUTE, HAIR_ROUTE, LAPTOP_ROUTE,
            FACE_1, FACE_2
        };

    /** Propiedades del programa. */
    public final static Properties PROPERTIES = new Properties();
}
