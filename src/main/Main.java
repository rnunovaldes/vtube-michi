package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;

import com.github.kwhat.jnativehook.GlobalScreen;
import com.github.kwhat.jnativehook.NativeHookException;

import main.engine.DeltaTimeManager;
import main.engine.detectors.Keyboard;
import main.engine.detectors.Microphone;
import main.engine.detectors.Mouse;
import main.engine.view.Window;

/**
 * Esta clase representa la entrada principal del programa.
 * Controla la inicialización de las configuraciones, la creación de
 * la ventana principal y la detección de dispositivos
 * (micrófono, teclado y ratón).
 */
public class Main {

    /**
     * El método principal que inicia el programa.
     * Carga las configuraciones, crea la ventana principal y realiza
     * la detección de dispositivos.
     *
     * @param args Los argumentos de la línea de comandos
     *             (no se utilizan en este programa).
     */
    public static void main(String[] args) {
        loadProperties();
        Window window = new Window();

        Microphone micDetector = null;
        if(Boolean.parseBoolean(Constants.PROPERTIES.getProperty("microphone_detection"))) {
            micDetector = new Microphone();
            micDetector.addObserver(window);
        }

        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException e) {
            e.printStackTrace();
        }

        Keyboard keyboard = null;
        if(Boolean.parseBoolean(Constants.PROPERTIES.getProperty("keyboard_detection"))) {
            keyboard = new Keyboard();
            keyboard.addObserver(window);
        }

        Mouse mouse = null;
        if(Boolean.parseBoolean(Constants.PROPERTIES.getProperty("mouse_detection"))) {
            mouse = new Mouse();
            mouse.addObserver(window);
        }

        DeltaTimeManager.getInstance().addObserver(window);
        if(micDetector != null) DeltaTimeManager.getInstance().addObserver(micDetector);
        if(keyboard != null) DeltaTimeManager.getInstance().addObserver(keyboard);
        if(mouse != null) DeltaTimeManager.getInstance().addObserver(mouse);
    }

    /**
     * Carga las configuraciones del archivo de propiedades.
     * Si el archivo no existe, se crea y se establecen valores
     * predeterminados. También arregla el archivo en caso de existir y
     * tener errores.
     */
    private static void loadProperties() {
        try(FileInputStream fileInputStream = new FileInputStream(Constants.CONFIG_FILE)) {
            Constants.PROPERTIES.load(fileInputStream);
        } catch (IOException e) {
            File file = new File(Constants.CONFIG_FILE);
            try {
                file.createNewFile();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

        String property = Constants.PROPERTIES.getProperty("microphone_detection");
        if(!isValidBoolean(property))
            Constants.PROPERTIES.setProperty("microphone_detection", "true");
        property = Constants.PROPERTIES.getProperty("microphone_threshold");
        if(!isValidByte(property))
            Constants.PROPERTIES.setProperty("microphone_threshold", "5");
        property = Constants.PROPERTIES.getProperty("microphone_channels");
        if(!isValidInt(property))
            Constants.PROPERTIES.setProperty("microphone_channels", "1");
        property = Constants.PROPERTIES.getProperty("keyboard_detection");
        property = Constants.PROPERTIES.getProperty("microphone_ups");
        if(!isValidInt(property))
            Constants.PROPERTIES.setProperty("microphone_ups", "10");
        if(!isValidBoolean(property))
            Constants.PROPERTIES.setProperty("keyboard_detection","true");
        property = Constants.PROPERTIES.getProperty("mouse_detection");
        if(!isValidBoolean(property))
            Constants.PROPERTIES.setProperty("mouse_detection","true");
        property = Constants.PROPERTIES.getProperty("window_width");
        if(!isValidInt(property))
            Constants.PROPERTIES.setProperty("window_width", "500");
        property = Constants.PROPERTIES.getProperty("window_height");
        if(!isValidInt(property))
            Constants.PROPERTIES.setProperty("window_height", "400");
        property = Constants.PROPERTIES.getProperty("window_location");
        if(property == null) Constants.PROPERTIES.setProperty("window_location", "se");
        property = Constants.PROPERTIES.getProperty("windows_taskbar_height");
        if(!isValidInt(property))
            Constants.PROPERTIES.setProperty("windows_taskbar_height", "50");
        property = Constants.PROPERTIES.getProperty("frames_per_second");
        if(!isValidInt(property))
            Constants.PROPERTIES.setProperty("frames_per_second", "60");

        try {
            FileWriter writer = new FileWriter(Constants.CONFIG_FILE);
            Constants.PROPERTIES.store(writer, "Config Properties");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Verifica si una cadena representa un valor booleano válido.
     *
     * @param value La cadena que se va a verificar.
     * @return {@code true} si la cadena es "true" o "false",
     *         {@code false} de lo contrario.
     */
    private static boolean isValidBoolean(String value) {
        return value != null &&
            (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false"));
    }

    /**
     * Verifica si una cadena representa un valor entero válido.
     *
     * @param value La cadena que se va a verificar.
     * @return {@code true} si la cadena es un entero válido,
     *         {@code false} de lo contrario.
     */
    private static boolean isValidInt(String value) {
        try {
            return Integer.parseInt(value) > 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * Verifica si una cadena representa un valor byte válido.
     *
     * @param value La cadena que se va a verificar.
     * @return {@code true} si la cadena es un byte válido,
     *         {@code false} de lo contrario.
     */
    private static boolean isValidByte(String value) {
        try {
            return Byte.parseByte(value) > 0;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
